
public class SpecialCharactersException extends RuntimeException {
	public SpecialCharactersException(String msg) {
		super(msg);
	}
}
