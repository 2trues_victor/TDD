import static org.junit.Assert.*;

import java.util.LinkedList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

public class TesteCamelCase {
	
	Conversor c;
	List<String> lista;
	List<String> respostas;
	
	@Before
	public void inicializaConversor() {
		c = new Conversor();
		respostas = new LinkedList<String>();
	}
	
	@Test
	public void converteUmaPalavra() {
		respostas.add("nome");
		
		lista = c.converterCamelCase("nome");
		assertEquals(respostas, lista);
	}

	@Test
	public void converteUmaPalavraComInicialMaiuscula() {
		respostas.add("nome");
		
		lista = c.converterCamelCase("Nome");
		assertEquals(respostas, lista);
	}
	
	
	@Test
	public void testesIta3() {
		respostas.add("nome");
		respostas.add("composto");
		
		lista = c.converterCamelCase("nomeComposto");
		assertEquals(respostas, lista);
	}
	
	@Test
	public void testesIta4() {
		respostas.add("nome");
		respostas.add("composto");
		
		lista = c.converterCamelCase("NomeComposto");
		assertEquals(respostas, lista);
	}
	
	@Test
	public void testesIta5() {
		respostas.add("CPF");
		
		lista = c.converterCamelCase("CPF");
		assertEquals(respostas, lista);
	}
	
	@Test
	public void testesIta6() {
		respostas.add("numero");
		respostas.add("CPF");
		
		lista = c.converterCamelCase("numeroCPF");
		assertEquals(respostas, lista);
	}
	
	@Test
	public void testesIta7() {
		respostas.add("numero");
		respostas.add("CPF");
		respostas.add("contribuinte");
		
		lista = c.converterCamelCase("numeroCPFContribuinte");
		assertEquals(respostas, lista);
	}
	
	@Test
	public void testesIta8() {
		respostas.add("recupera");
		respostas.add("10");
		respostas.add("primeiros");
		
		lista = c.converterCamelCase("recupera10Primeiros");
		assertEquals(respostas, lista);
	}
	
	@Test(expected=IniciaCNumException.class)
	public void testesIta9() {
		lista = c.converterCamelCase("10Primeiros");
	}
	
	@Test(expected=SpecialCharactersException.class)
	public void testesIta10() {
		lista = c.converterCamelCase("nome#Composto");
	}
	

	@Test
	public void testesIta() {

		List<String> testes = new LinkedList<String>();
		testes.add("nome");
		testes.add("Nome");
		testes.add("nomeComposto");
		testes.add("NomeComposto");
		testes.add("CPF");
		testes.add("numeroCPF");
		testes.add("numeroCPFContribuinte");
		testes.add("recupera10Primeiros");
		testes.add("10Primeiros");
		testes.add("nome#Composto");
		
		List<String> r1 = new LinkedList<String>();
			r1.add("nome");
		List<String> r2 = new LinkedList<String>();
			r2.add("nome");
		List<String> r3 = new LinkedList<String>();
			r3.add("nome");
			r3.add("composto");
		List<String> r4 = new LinkedList<String>();
			r4.add("nome");
			r4.add("composto");
		List<String> r5 = new LinkedList<String>();
			r5.add("CPF");
		List<String> r6 = new LinkedList<String>();
			r6.add("numero");
			r6.add("CPF");
		List<String> r7 = new LinkedList<String>();
			r7.add("numero");
			r7.add("CPF");
			r7.add("contribuinte");
		List<String> r8 = new LinkedList<String>();
			r8.add("recupera");
			r8.add("10");
			r8.add("primeiros");
		
		List<List<String>> respostas = new LinkedList<List<String>>();

		respostas.add(r1);
		respostas.add(r2);
		respostas.add(r3);
		respostas.add(r4);
		respostas.add(r5);
		respostas.add(r6);
		respostas.add(r7);
		respostas.add(r8);
		
		for (int i = 0 ; i < respostas.size() ; i++) {
			List<String> lista = c.converterCamelCase(testes.get(i));
			assertEquals(respostas.get(i), lista);
		}
		
	}
}
