import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;



public class Conversor {

	public static List<String> converterCamelCase(String original) {
		
		// Se começa com numero gera excecao
		if (Character.isDigit(original.charAt(0))) {
			throw new IniciaCNumException("Invalido - Não deve começar com números.");
		}
		
		List<String> lista = new ArrayList<String>();
		
		int inicioPalavra = 0;
		
		// Percorre cada letra da string
		for (int i=0 ; i<original.length() ; i++) {

			Character letraAtual = original.charAt(i);

			if (!Character.isDigit(letraAtual) && !Character.isLetter(letraAtual)) {
				throw new SpecialCharactersException("Inválido - caracteres especiais não são permitidos, somente letras e números");			
			}
			
			if (i == 0) {
				continue;
			}

			Character letraAnterior = original.charAt(i-1);
			
			// CASO 1 - ClienteCPF - anterior minuscula
			if (Character.isUpperCase(letraAtual) && !Character.isUpperCase(letraAnterior)) {
				
				// separa a palavra
				String palavra = Character.toLowerCase(original.charAt(inicioPalavra)) + original.substring(inicioPalavra + 1, i);
				
				// adiciona a palavra
				lista.add(palavra);
				
				inicioPalavra = i;
			}
			
			// CASO 2 -> CPFCliente - atual minuscula e anterior maiuscula, i-1 proíbe que sejam adicionadas palavras vazias
			if (inicioPalavra != i-1 && !Character.isUpperCase(letraAtual) && Character.isUpperCase(letraAnterior)) {
				
				// adiciona a sigla
				lista.add(original.substring(inicioPalavra, i-1));
				
				inicioPalavra = i-1;
			}
			
			// CASO 3 -> colocar numeros como uma palavra
			if (inicioPalavra != i-1 && Character.isDigit(letraAtual)) {
				// adiciona a sigla
				lista.add(original.substring(inicioPalavra, i));
				
				inicioPalavra = i;
								
				while (Character.isDigit(original.charAt(i))) {
					
					i++;
					
				}
				
				lista.add(original.substring(inicioPalavra, i));
				
				inicioPalavra = i;
			}
			
		}
		
		// arruma ultima palavra
		if (original.length()>1 && !Character.isUpperCase(original.charAt(inicioPalavra+1))) {
			
			String palavra = Character.toLowerCase(original.charAt(inicioPalavra)) + original.substring(inicioPalavra + 1, original.length());
			
			lista.add(palavra);
			
		} else {
			
			lista.add(original.substring(inicioPalavra, original.length()));			
			
		}
		
		return lista;
		
	}

}
